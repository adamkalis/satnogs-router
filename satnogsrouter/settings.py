"""
SatNOGS Router settings file
"""
from os import environ

from dotenv import load_dotenv


def _cast_or_none(func, value):
    try:
        return func(value)
    except (ValueError, TypeError):
        return None


try:
    load_dotenv()
except EnvironmentError:
    # XXX: Workaround for readthedocs  # pylint: disable=fixme
    pass

# ZeroMQ Proxy URIs
ZEROMQ_PROXY_XSUB_URI = environ.get('ZEROMQ_PROXY_XSUB_URI', 'tcp://0.0.0.0:5555')
ZEROMQ_PROXY_XPUB_URI = environ.get('ZEROMQ_PROXY_XPUB_URI', 'tcp://0.0.0.0:5560')
ZEROMQ_MONITOR_PUB_URI = environ.get('ZEROMQ_MONITOR_PUB_URI', 'tcp://0.0.0.0:5565')
ZEROMQ_MONITOR_SUB_URI = environ.get('ZEROMQ_MONITOR_SUB_URI', 'tcp://0.0.0.0:5565')

# Logging configuration
LOG_FORMAT = '%(asctime)s %(name)s - %(levelname)s - %(message)s'
LOG_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
LOG_LEVEL = environ.get('SATNOGS_LOG_LEVEL', 'INFO')
