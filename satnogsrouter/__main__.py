"""
SatNOGS Router module main function
"""
import sys

import satnogsrouter

if __name__ == '__main__':
    sys.exit(satnogsrouter.main())
